En PHP, existen varios tipos de bucles, pero los más comunes son el bucle `for`, el bucle `while` y el bucle `foreach`.

## For: 

El bucle `for` se utiliza cuando conoces el número exacto de repeticiones que deseas realizar. Tiene tres partes fundamentales: la inicialización, la condición y la actualización. El bucle `for` se ejecuta mientras la condición sea verdadera.

``` php
for ($i = 0; $i < 5; $i++) {
    // Código a ejecutar en cada iteración
    echo $i;
}
```

En este ejemplo, el bucle `for` se ejecuta cinco veces. Primero, se inicializa la variable `$i` en 0. Luego, se verifica la condición `$i < 5`. Si es verdadera, se ejecuta el bloque de código dentro del bucle y, al final de cada iteración, se actualiza la variable `$i` incrementándola en 1.

## While:

El bucle `while` se utiliza cuando no se conoce el número exacto de repeticiones y se ejecuta mientras se cumpla una condición.

``` php
$i = 0;
while ($i < 5) {
    // Código a ejecutar en cada iteración
    echo $i;
    $i++;
}
```

En este ejemplo, el bucle `while` se ejecuta mientras la condición `$i < 5` sea verdadera. Primero, se inicializa la variable `$i` en 0. Luego, se verifica la condición en cada iteración. Si es verdadera, se ejecuta el bloque de código dentro del bucle y se actualiza la variable `$i`. Es importante asegurarse de que la condición finalmente se vuelva falsa en algún momento para evitar un bucle infinito.

## Do while:

El bucle `do-while` es similar al bucle `while`, pero la condición se verifica al final del bloque de código, lo que garantiza que el bloque se ejecute al menos una vez.

``` php
$i = 0;
do {
    // Código a ejecutar en cada iteración
    echo $i;
    $i++;
} while ($i < 5);
```


## Foreach:

El bucle `foreach` se utiliza para iterar sobre los elementos de un array o una colección de datos.

``` php
$frutas = array('manzana', 'banana', 'cereza');
foreach ($frutas as $fruta) {
    // Código a ejecutar en cada iteración
    echo $fruta;
}
```

En este ejemplo, el bucle `foreach` recorre cada elemento del array `$frutas` y asigna el valor de cada elemento a la variable `$fruta` en cada iteración. El bloque de código dentro del bucle se ejecuta una vez por cada elemento del array.


# Control de [[Bucles]] 

En PHP, puedes controlar los bucles de varias maneras para ajustar su comportamiento según tus necesidades. Estas técnicas te permiten alterar el flujo de ejecución y controlar cuántas veces o bajo qué condiciones se repite un bucle. 
Estas son algunas formas comunes de controlar los bucles:

## **break:** 

La declaración `break` se utiliza para salir inmediatamente de un bucle, independientemente de si se ha completado todas las iteraciones previstas.

```php
for ($i = 1; $i <= 10; $i++) {
    if ($i == 5) {
        break; // Salir del bucle cuando $i sea igual a 5
    }
    echo "$i ";
}
// Salida: 1 2 3 4
```

## **continue:** 

La declaración `continue` se utiliza para detener la iteración actual y pasar a la siguiente iteración del bucle.

```php
for ($i = 1; $i <= 5; $i++) {
    if ($i == 3) {
        continue; // Saltar la iteración cuando $i sea igual a 3
    }
    echo "$i ";
}
// Salida: 1 2 4 5
```




